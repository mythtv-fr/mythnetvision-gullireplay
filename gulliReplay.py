#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

__title__ ="Gulli Replay";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="0.9"
__usage__ ='''
Gulli Replay grabber for MythNetVision 0.25

Usage: %(file)s [option][<search>]
Version: %(version)s Author: %(author)s
Homepage: http://mythtv-fr.org/wiki/mythnetvision/gulli_replay

For details on the MythTV Netvision plugin see the wiki page at:
http://www.mythtv.org/wiki/MythNetvision

Options:
  -h, --help            show this help message and exit
  -v, --version         Display grabber name and supported options
  -S:, --search=        Search <search> for videos
  -T, --treeview        Display a Tree View of a sites videos
  -p, --page            Page of the search
'''%{'file':__file__, 'version':__version__,'author':__author__}

import sys, os, locale
from getopt import getopt
from xml.dom import minidom
from BeautifulSoup import BeautifulSoup
from urllib import urlopen

import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s')
logger = logging.getLogger(__title__)

reload(sys)
sys.setdefaultencoding('utf-8')

# INIT
# Url de base
urlSite = 'http://replay.gulli.fr/'
programsType = [
        ('http://replay.gulli.fr/replay/dessins-animes', 'Dessins animés'),
        ('http://replay.gulli.fr/replay/emissions','Émissions'),
        ('http://replay.gulli.fr/replay/series','Séries et films')
    ]

urlSearch = 'http://replay.gulli.fr/recherche/replay?searchText=%s'
# Url du lecteur
#   - %(id)s : id de la vidéo
#   - %(url)s : url du fichier flv
# Lecteur externe devant être utilisé ('' pour le lecteur interne)
player = False
# Argument du lecteur (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
playerargs = False
# "Téléchargeur" ('' pour le téléchargeur par défaut)
download = False
# Argument du "téléchargeur" (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
downloadargs = False
# Description de channel
channel = {
        'title' : __title__,
        'link' : urlSite,
        'description' : 'Savourez le meilleur de Gulli à volonté !'
    }


def MinidomDocument_createNode(self,tag,text='',attributes={}) :
    '''
        Retourne un noeud avec pour tag "tag", comme text "text" ou comme
        attributs le dico "attributes" ({attrName: attrValue})
    '''
    node = self.createElement(tag)
    if text :
        node.appendChild(self.createTextNode(text))
    for (name,value) in attributes.items() :
        node.setAttribute(name,value)
    return node
setattr(minidom.Document,'createNode',MinidomDocument_createNode)


def MinidomNode_getChildByAttribute(self,attrname, attrvalue) :
    '''
        Retourne le 1er enfant directe qui a pour l'attribut "attrname" la
        valeur "attrvalue". Si aucun retourne False.
    '''
    for node in self.childNodes :
        if node.hasAttribute(attrname) :
            if node.getAttribute(attrname) == attrvalue :
                return node
    return None
setattr(minidom.Node,'getChildByAttribute',MinidomNode_getChildByAttribute)


def MinidomDocument_createItemNodeFromSoup(self,soup,titleWithSerie=True) :
    '''
        Retourne un noeud "item" avec un élément à partir d'un neud "li"
        beautifulSoup
    '''
    itemOutputNode = self.createElement('item')

    title= ' - '.join([s.strip() for s in (soup.toEncoding(soup.find('p').find('span'))[6:-7].split('<br />'))])
    if titleWithSerie :
        title = title + ': ' + soup.find('p').find('strong').string
    itemOutputNode.appendChild(self.createNode('title',title))
    itemOutputNode.appendChild(self.createNode('description',title))

    mediaNodeSoup = soup.find('div',attrs={'class':'img'})
    link = mediaNodeSoup.find('a').get('href')
    itemOutputNode.appendChild(self.createNode('link',link))

    mediaOutputNode = itemOutputNode.appendChild(self.createNode('media:group'))
    mediaOutputNode.appendChild(self.createNode(
            tag='media:thumbnail',
            attributes={
                    'url':mediaNodeSoup.find('img').get('src')
        }))
    mediaOutputNode.appendChild(self.createNode(
            tag='media:content',
            attributes={
                    'url':link,
        }))
    if player :
        itemOutputNode.appendChild(self.createNode('player',player))
    if playerargs :
        itemOutputNode.appendChild(self.createNode('playerargs',playerargs))
    if download :
        itemOutputNode.appendChild(self.createNode('download',download))
    if downloadargs :
        itemOutputNode.appendChild(self.createNode('downloadargs',downloadargs))

    return itemOutputNode
setattr(minidom.Node,'createItemNodeFromSoup',MinidomDocument_createItemNodeFromSoup)



def version(tree,search) :
    '''
        Retourne le noeud racine permettant a mythtv de reconnaitre le nom,
        et les fonction de ce grabbeur
    '''
    domOutput = minidom.Document()
    rootNode = domOutput.appendChild(domOutput.createNode('grabber'))
    rootNode.appendChild(domOutput.createNode('name',__title__))
    rootNode.appendChild(domOutput.createNode('command',__file__))
    rootNode.appendChild(domOutput.createNode('author',__author__))
    rootNode.appendChild(domOutput.createNode('thumbnail','gulliReplay.png'))
    rootNode.appendChild(domOutput.createNode('type','video'))
    rootNode.appendChild(domOutput.createNode('description',channel['description']))
    rootNode.appendChild(domOutput.createNode('version',__version__))
    rootNode.appendChild(domOutput.createNode('tree','true'))
    rootNode.appendChild(domOutput.createNode('search','true'))
    return rootNode.toxml()


def domTreeview() :
    '''
        Retourne un domDocument pour la vue arbre de MythNetVision
    '''
    # dom xml de sortie
    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://backend.userland.com/creativeCommonsRssModule',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                }
        ))
    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    #channel type
    for (programTypeUrl,programTypeName) in programsType :
        directoryTypeOutputNode = channelOutputNode.appendChild(domOutput.createNode(
                tag = 'directory',
                attributes = {
                        'name':programTypeName,
                        #'thumbnail':
                    }
            ))
        soup = BeautifulSoup(urlopen(programTypeUrl).read())

        for soupListeResultat in soup.findAll('ul',attrs={'class':'liste_resultats'}) :
            for soupItem in soupListeResultat.findAll('li') :
                serieName = soupItem.find('p').find('strong').string
                directorySerieOutputNode = directoryTypeOutputNode.getChildByAttribute('name',serieName)
                if not directorySerieOutputNode :
                    directorySerieOutputNode = directoryTypeOutputNode.appendChild(domOutput.createNode(
                            tag = 'directory',
                            attributes = {
                                    'name':serieName,
                                    #'thumbnail':
                                }
                        ))
                directorySerieOutputNode.appendChild(domOutput.createItemNodeFromSoup(soupItem,titleWithSerie=False))

    return domOutput


def domSearch (text,page) :
    '''
        Retourne un domDocument pour la recherche de MythNetVision
    '''


    domOutput = minidom.Document()
    nodeOutputRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://cnettv.com/mrss/',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                    'xmlns:mythtv':'http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format'
                }))

    channelOutputNode = nodeOutputRss.appendChild(domOutput.createNode('channel'))
    channelOutputNode.appendChild(domOutput.createNode('title',__title__))
    channelOutputNode.appendChild(domOutput.createNode('link',channel['link']))
    channelOutputNode.appendChild(domOutput.createNode('description',channel['description']))

    url = urlSearch%text
    logger.info('Recherche sur "%s"'%url)
    soup = BeautifulSoup(urlopen(url).read())

    if soup.find('div',id='wrapper').find(attrs={'class':'main_title'}).find('span').string != 'il n\'y a pas de programme pour cette recherche. Essayez une recherche moins sp&eacute;cifique !' :
        for soupListeResultat in soup.findAll('ul',attrs={'class':'liste_resultats'}) :
            for soupItem in soupListeResultat.findAll('li') :
                channelOutputNode.appendChild(domOutput.createItemNodeFromSoup(soupItem))

    return domOutput


if __name__ == "__main__" :

    opts,args = getopt(sys.argv[1:], "hvS:Tp:",["help","version","search=","treeview","page="])
    if len(opts) == 0 :
        sys.stdout.write(__usage__)
        sys.exit(0)

    search = False
    page = 1
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(0)
        elif opt in ("-v", "--version"):
            sys.stdout.write(version(tree=True, search=True))
            sys.exit(0)
        elif opt in ("-S", "--search"):
            search = arg
        elif opt in ("-p", "--page"):
            page = int(arg)
        elif opt in ("-T", "--treeview"):
            sys.stdout.write(domTreeview().toxml())
            sys.exit(0)

    if search :
        sys.stdout.write(domSearch(search,page).toxml())
        sys.exit(0)
